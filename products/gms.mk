#
# Copyright (C) 2020-2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_PACKAGES += \
    BananaConfigOverlay \
    GmsConfigOverlayASI \
    GmsConfigOverlayTurbo \
    GoogleSettingsOverlay \
    PixelConfigOverlay2018 \
    PixelConfigOverlay2019 \
    PixelConfigOverlay2019Midyear \
    PixelConfigOverlay2021 \
    PixelSetupWizardOverlay \
    PixelSetupWizardOverlay2019 \
    PixelSetupWizardStringsOverlay

PRODUCT_PRODUCT_PROPERTIES += \
    ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural \
    ro.carriersetup.vzw_consent_page=true \
    ro.opa.eligible_device=true \
    ro.setupwizard.enterprise_mode=1 \
    ro.setupwizard.esim_cid_ignore=00000001 \
    setupwizard.feature.baseline_setupwizard_enabled=true \
    setupwizard.feature.enable_wifi_tracker=true \
    setupwizard.feature.show_pai_screen_in_main_flow.carrier1839=false \
    setupwizard.feature.show_pixel_tos=true \
    setupwizard.feature.skip_button_use_mobile_data.carrier1839=true \
    setupwizard.theme=glif_v3_light

ifeq ($(strip $(WITH_GAPPS)), true)
    ifeq ($(strip $(BUILD_CORE_GAPPS)), true)
        TARGET_USE_GOOGLE_TELEPHONY ?= false
        BUILD_CORE_GAPPS_EXTRA ?= false
    else
        TARGET_USE_GOOGLE_TELEPHONY ?= true
        BUILD_CORE_GAPPS_EXTRA ?= true
    endif

    ifeq ($(strip $(TARGET_USE_GOOGLE_TELEPHONY)), true)
        PRODUCT_PACKAGES += \
            GmsConfigOverlayTelecom \
            GmsConfigOverlayTeleService \
            GmsConfigOverlayComms \
            GoogleDialerOverlay
    endif

    # GMS properties
    PRODUCT_PRODUCT_PROPERTIES += \
        ro.com.google.ime.theme_id=5

    ifneq ($(strip $(BUILD_CORE_GAPPS)), true)
        # GMS RRO overlay packages
        PRODUCT_PACKAGES += \
            GmsConfigOverlayCommon \
            GmsConfigOverlayContactsProvider \
            GmsConfigOverlayGeotz \
            GmsConfigOverlayGSA \
            GmsConfigOverlayPersonalSafety \
            GmsConfigOverlayPhotos \
            GmsConfigOverlaySettings \
            GmsConfigOverlaySettingsProvider \
            GmsConfigOverlaySystemUI \
            GmsConfigOverlayVAS

        # Pixel RRO overlay packages
        PRODUCT_PACKAGES += \
            GoogleConfigOverlay \
            PixelConfigOverlayCommon \
            PixelConfigOverlayWallpaper \
            PixelDocumentsUIGoogleOverlay

        # Pixel properties
        PRODUCT_PRODUCT_PROPERTIES += \
            ro.com.google.ime.kb_pad_port_b=10 \
            ro.com.google.ime.system_lm_dir=/product/usr/share/ime/google/d3_lms
    endif
endif

$(call inherit-product, vendor/gms/common/common-vendor.mk)
